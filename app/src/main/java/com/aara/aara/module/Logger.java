package com.aara.aara.module;

import android.util.Log;

public class Logger {

    public static final String CONSTRUCTOR = "<<constructor>>";

    public static void log(String tag, String msg) {
        if(Config.DEBUG) {
            Log.d(tag, msg);
        }
    }

}
