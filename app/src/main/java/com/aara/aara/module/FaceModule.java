package com.aara.aara.module;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.aara.aara.R;
import com.microsoft.projectoxford.face.FaceServiceClient;
import com.microsoft.projectoxford.face.FaceServiceRestClient;
import com.microsoft.projectoxford.face.contract.AddPersistedFaceResult;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FaceList;
import com.microsoft.projectoxford.face.contract.FaceMetadata;
import com.microsoft.projectoxford.face.contract.FaceRectangle;
import com.microsoft.projectoxford.face.contract.SimilarPersistedFace;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.UUID;

public class FaceModule {

    private static final String TAG = "FaceModule";

    public static FaceServiceClient faceServiceClient;

    public static void init(Context ctx) {
        Logger.log(TAG, "init");
        faceServiceClient = new FaceServiceRestClient(ctx.getResources().getString(R.string.ms_face_url), ctx.getResources().getString(R.string.ms_face_sub_key));
    }

    public static void detect(final Bitmap img, final Next nxt) {
        Logger.log(TAG, "detect");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        new AsyncTask<InputStream, String, Face[]>() {

            @Override
            protected Face[] doInBackground(InputStream... params) {
                try {
                    Face[] result = faceServiceClient.detect(params[0], true, false, null);
                    if (result == null) {
                        return null;
                    }
                    return result;
                } catch (Exception e) {
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Face[] result) {
                try {
                    Logger.log(TAG, String.valueOf(result.length));
                } catch(Exception e) {
                    e.printStackTrace();
                }
                nxt.next(result);
            }

        }.execute(inputStream);
    }

    public static void findSimilar(final Context ctx, final UUID faceId, final Next nxt) {
        Logger.log(TAG, "findSimilar");
        new AsyncTask<String, String, SimilarPersistedFace[]>() {

            @Override
            protected SimilarPersistedFace[] doInBackground(String... params) {
                try {
                    SimilarPersistedFace [] result = faceServiceClient.findSimilar(faceId, ctx.getResources().getString(R.string.ms_face_list_id), 1);
                    if (result == null) {
                        return null;
                    }
                    return result;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(SimilarPersistedFace[] result) {
                nxt.next(result);
            }

        }.execute("");
    }

    public static void addFace(final Context ctx, final Bitmap img, final String info, final FaceRectangle rect, final Next nxt) {
        Logger.log(TAG, "addFace");
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        img.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        new AsyncTask<InputStream, String, AddPersistedFaceResult>() {

            @Override
            protected AddPersistedFaceResult doInBackground(InputStream... params) {
                try {
                    AddPersistedFaceResult result = faceServiceClient.AddFaceToFaceList(ctx.getResources().getString(R.string.ms_face_list_id), params[0], info, rect);
                    if (result == null) {
                        return null;
                    }
                    return result;
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(AddPersistedFaceResult result) {
                nxt.next(result);
            }

        }.execute(inputStream);
    }

    public static void getUser(final Context ctx, final UUID id, final Next nxt) {
        Logger.log(TAG, "getUser");
        new AsyncTask<String, String, JSONObject>() {

            @Override
            protected JSONObject doInBackground(String... params) {
                try {
                    OkHttpClient client = new OkHttpClient();
                    Request request = new Request.Builder()
                            .url("https://southeastasia.api.cognitive.microsoft.com/face/v1.0/facelists/" + ctx.getResources().getString(R.string.ms_face_list_id))
                            .get()
                            .addHeader("content-type", "application/json")
                            .addHeader("ocp-apim-subscription-key", ctx.getResources().getString(R.string.ms_face_sub_key))
                            .build();
                    Response response = client.newCall(request).execute();
                    JSONObject json = new JSONObject(response.body().string());
                    JSONArray arr = json.getJSONArray("persistedFaces");
                    String pId = id.toString();
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject obj = arr.getJSONObject(i);
                        JSONObject data = new JSONObject(obj.getString("userData"));
                        String faceId = obj.getString("persistedFaceId");
                        if (pId.equals(faceId)) {
                            return data;
                        }
                    }
                    /*
                    FaceList l = faceServiceClient.getFaceList(ctx.getResources().getString(R.string.ms_face_list_id));
                    Logger.log(TAG, l.toString());
                    Logger.log(TAG, l.name);

                    Logger.log(TAG, String.valueOf(l.faces.length));
                    for (FaceMetadata face : l.faces) {
                        String data = face.userData;
                        String faceId = face.faceId.toString();
                        if (pId.equals(faceId)) {
                            return new JSONObject(data);
                        }
                    }
                    */
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject result) {
                nxt.next(result);
            }

        }.execute("");
    }

}
