package com.aara.aara.module;


public class Net {

    private static final String TAG = "Net";
/*
    public static void post(final String url, final JSONObject json, final Next success, final Next fail) {
        Logger.log(TAG, "post");
        try {
            new AsyncTask<String, Void, String>() {

                boolean error = false;

                protected String doInBackground(String... str) {
                    Logger.log(TAG, "doInBackground");
                    try {
                        HttpClient httpClient = HttpClientBuilder.create().build();
                        HttpPost request = new HttpPost(url);
                        StringEntity params = new StringEntity(json.toString());
                        request.addHeader("content-type", "application/json");
                        request.setEntity(params);
                        HttpResponse response = httpClient.execute(request);
                        String res = EntityUtils.toString(response.getEntity());
                        return res;
                    } catch(Exception e) {
                        e.printStackTrace();
                        error = true;
                    }
                    return "";
                }

                protected void onPostExecute(String res) {
                    Logger.log(TAG, "onPostExecute");
                    Logger.log(TAG, res);
                    if(error) {
                        fail.next(null);
                    } else {
                        success.next(res);
                    }
                }

            }.execute();
        }catch (Exception e) {
            e.printStackTrace();
            fail.next(null);
        }
    }

    public static void post(final String url, final String name, final Bitmap img, final Next success, final Next fail) {
        Logger.log(TAG, "post");
        try {
            new AsyncTask<String, Void, String>() {

                boolean error = false;

                protected String doInBackground(String... str) {
                    Logger.log(TAG, "doInBackground");
                    try {
                        HttpClient httpClient = HttpClientBuilder.create().build();
                        HttpPost request = new HttpPost(url);
                        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                        ByteArrayOutputStream bos = new ByteArrayOutputStream();
                        img.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                        byte [] data = bos.toByteArray();
                        builder.addPart(name, new ByteArrayBody(data, "image/jpeg", "img.jpg"));
                        HttpEntity entity = builder.build();
                        request.setEntity(entity);
                        HttpResponse response = httpClient.execute(request);
                        String res = EntityUtils.toString(response.getEntity());
                        return res;
                    } catch(Exception e) {
                        e.printStackTrace();
                        error = true;
                    }
                    return "";
                }

                protected void onPostExecute(String res) {
                    Logger.log(TAG, "onPostExecute");
                    Logger.log(TAG, res);
                    if(error) {
                        fail.next(null);
                    } else {
                        success.next(res);
                    }
                }

            }.execute();
        }catch (Exception e) {
            e.printStackTrace();
            fail.next(null);
        }
    }
   */

}
