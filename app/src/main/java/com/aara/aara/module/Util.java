package com.aara.aara.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.aara.aara.R;

public class Util {

    private static final String TAG = "Util";

    public static void toast(Context context, String msg) {
        Logger.log(TAG, "toast");
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void toast(Context context, int msg) {
        Logger.log(TAG, "toast");
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String readPreference(Context context, String key) {
        Logger.log(TAG, "readPreference");
        String def = "";
        try {
            SharedPreferences sharedPref = context.getSharedPreferences(context.getResources().getString(R.string.shared_pref_context), Context.MODE_PRIVATE);
            return sharedPref.getString(key, def);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return def;
    }

    public static void writePreference(Context context, String key, String val) {
        Logger.log(TAG, "writePreference");
        try {
            SharedPreferences sharedPref = context.getSharedPreferences(context.getResources().getString(R.string.shared_pref_context), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(key, val);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
