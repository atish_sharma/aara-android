package com.aara.aara.module;

public abstract class Next {

    public abstract void next(Object obj);

}
