package com.aara.aara.model;

import android.content.Context;
import android.widget.Toast;

import com.aara.aara.module.Logger;
import com.aara.aara.module.Util;

import org.json.JSONObject;

public class User {

    private static final String TAG = "User";

    private static User user = null;

    private String userId;
    private String name;
    private String phone;
    private String email;
    private String status;
    private String address;
    public JSONObject relations;

    public static User getInstance(Context ctx) {
        if (user == null) {
            user = new User();
            user.relations = new JSONObject();
        }
        user.update(ctx);
        return user;
    }

    private User() {
        Logger.log(TAG, Logger.CONSTRUCTOR);
    }

    public void persist(Context ctx) {
        Logger.log(TAG, "persist");
        Util.writePreference(ctx, "user_id", userId);
        Util.writePreference(ctx, "name", name);
        Util.writePreference(ctx, "phone", phone);
        Util.writePreference(ctx, "email", email);
        Util.writePreference(ctx, "status", status);
        Util.writePreference(ctx, "address", address);
        try {
            Util.writePreference(ctx, "rel", relations.toString());
            // Util.toast(ctx, relations.toString());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void update(Context ctx) {
        Logger.log(TAG, "update");
        userId = Util.readPreference(ctx, "user_id");
        name = Util.readPreference(ctx, "name");
        phone = Util.readPreference(ctx, "phone");
        email = Util.readPreference(ctx, "email");
        status = Util.readPreference(ctx, "status");
        address = Util.readPreference(ctx, "address");
        try {
            relations = new JSONObject(Util.readPreference(ctx, "rel"));
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void logout(Context ctx) {
        Logger.log(TAG, "logout");
        clear();
        persist(ctx);
    }

    public boolean loggedIn() {
        Logger.log(TAG, "loggedIn");
        return userId != null && userId != "";
    }

    private void clear() {
        Logger.log(TAG, "clear");
        userId = null;
        name = null;
        phone = null;
        email = null;
        status = null;
        address = null;
        relations = new JSONObject();
    }

    public String getJson() {
        Logger.log(TAG, "getJson");
        JSONObject obj = new JSONObject();
        try {
            obj.put("name", name);
            obj.put("phone", phone);
            obj.put("email", email);
            obj.put("status", status);
            obj.put("address", address);
            return obj.toString();
        } catch(Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
