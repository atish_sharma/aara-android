package com.aara.aara.controller.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.Logger;

import org.json.JSONObject;

import static java.security.AccessController.getContext;

public class UserProfileActivity extends AppCompatActivity {

    private static final String TAG = "UserProfileActivity";

    private ListView mList;
    private FloatingActionButton mFab;
    private User mUser;
    private String id;
    private String items [] = {"", "", "", "", ""};
    private ArrayAdapter<String> adapter;
    Context cont = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.log(TAG, "onCreate");
        setContentView(R.layout.activity_user_profile);
        mUser = User.getInstance(getBaseContext());
        mList = (ListView) findViewById(R.id.list);
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(cont);
                builder.setTitle("Enter relation");

// Set up the input
                final EditText input = new EditText(getApplicationContext());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

// Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String txt = input.getText().toString();
                        try {
                            mUser.relations.put(id, txt);
                            items[4] = "Relation : "+txt;
                            adapter.notifyDataSetChanged();
                            mFab.setVisibility(View.GONE);
                            mUser.persist(getBaseContext());
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                /*
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Enter relation");
                // I'm using fragment here so I'm using getView() to provide ViewGroup
                // but you can provide here any other instance of ViewGroup from your Fragment / Activity
                // View viewInflated = LayoutInflater.from(getApplicationContext()).inflate(R.layout.text_inpu_password, (ViewGroup) findViewById(android.R.id.content), false);
                // Set up the input
                LayoutInflater inflater = getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.text_inpu_password, null);
                builder.setView(dialogView);
                // builder.setView(R.layout.text_inpu_password);
                AlertDialog d = builder.create();
                final EditText input = (EditText) dialogView.findViewById(R.id.input);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                // builder.setView(viewInflated);

                // Set up the buttons
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        String txt = input.getText().toString();
                        try {
                            mUser.relations.put(id, txt);
                            items[4] = txt;
                            adapter.notifyDataSetChanged();
                            mFab.setVisibility(View.GONE);
                        } catch(Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                */
            }
        });
        String relation = "UNKNOWN";
        getSupportActionBar().setTitle("Profile");
        try {
            String info = getIntent().getStringExtra("info");
            JSONObject json = new JSONObject(info);
            String name = json.getString("name");
            String address = json.getString("address");
            String status = json.getString("status");
            String phone = json.getString("phone");
            String email = json.getString("email");
            id = json.getString("id");
            getSupportActionBar().setTitle(name);
            if (mUser.relations.has(id)) {
                relation = mUser.relations.getString(id);
            } else {
                mFab.setVisibility(View.VISIBLE);
            }
            items[0] = "Phone : " + phone;
            items[1] = "Email : " + email;
            items[2] = "Address : " + address;
            items[3] = "Status : " + status;
            items[4] = "Relation : " + relation;
            adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, items);
            mList.setAdapter(adapter);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
