package com.aara.aara.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.FaceModule;
import com.aara.aara.module.Logger;
import com.aara.aara.module.Next;
import com.aara.aara.module.Util;
import com.google.gson.JsonObject;
import com.microsoft.projectoxford.face.contract.AddPersistedFaceResult;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import clarifai2.api.ClarifaiBuilder;
import clarifai2.api.ClarifaiClient;
import clarifai2.api.ClarifaiResponse;
import clarifai2.dto.input.ClarifaiImage;
import clarifai2.dto.input.ClarifaiInput;
import okhttp3.OkHttpClient;

public class ObjectDetectionActivity extends AppCompatActivity {
    private static final String TAG = "ObjectDetection";

    public static final int REQUEST_CAMERA = 1888;
    public static final int REQUEST_DETAILS = 1889;
    private Context mContext;
    public ClarifaiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_detection);
        mContext = getBaseContext();

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, REQUEST_CAMERA);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.log(TAG, "Activivtyy");
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");

            String filePath = saveToInternalStorage(photo);

            File fileObject = new File(filePath, "profile.jpg");
            Logger.log(TAG, fileObject.getAbsolutePath());
            client = new ClarifaiBuilder("e2908a6f755140bca240bcf0bd5bfc2c")
                    .client(new OkHttpClient())
                    .buildSync();

            AsyncTask<File, String, ClarifaiResponse> doTask =
                    new AsyncTask<File, String, ClarifaiResponse>() {
                        @Override
                        protected ClarifaiResponse doInBackground(File... params) {

                            return client.getDefaultModels().generalModel().predict()
                                    .withInputs(
                                            ClarifaiInput.forImage(ClarifaiImage.of(params[0]))
                                    )
                                    .executeSync();
                        }

                        @Override
                        protected void onPostExecute(ClarifaiResponse result) {
                            try{
                                JSONObject responseObject = new JSONObject(result.rawBody());
                                Logger.log(TAG, responseObject.toString());

                                JSONArray obj2 = responseObject.getJSONArray("outputs").getJSONObject(0).getJSONObject("data").getJSONArray("concepts");
//                                JSONObject obj2 = responseObject.getJSONObject("data");

                                Logger.log(TAG, obj2.toString());


                                String[] productList = new String[5];
                                for (int i = 0,  j=0; j < 5 && i < obj2.length(); i++) {
                                    JSONObject obj = obj2.getJSONObject(i);
                                    String product = obj.getString("name");
                                    Logger.log(TAG, product);
                                    if (product.equals("no person")) {

                                    } else{
                                        productList[j] = product;
                                        j++;
                                    }

                                }
                                Logger.log(TAG, productList.toString());
                                Intent i = new Intent(ObjectDetectionActivity.this, OptionsActivity.class);
                                i.putExtra("productList", productList);
                                startActivityForResult(i, REQUEST_DETAILS);
                            }
                            catch (Exception e)
                            {
                                Logger.log(TAG, e.getMessage());
                            }


                        }
                    };
            doTask.execute(fileObject);

        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getApplicationContext());
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        File mypath=new File(directory,"profile.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return directory.getAbsolutePath();
    }
}
