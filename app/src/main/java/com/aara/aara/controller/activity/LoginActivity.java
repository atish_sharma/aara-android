package com.aara.aara.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.FaceModule;
import com.aara.aara.module.Logger;
import com.aara.aara.module.Next;
import com.aara.aara.module.Util;

import com.microsoft.projectoxford.face.contract.AddPersistedFaceResult;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FaceRectangle;
import com.microsoft.projectoxford.face.contract.SimilarPersistedFace;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONObject;

import java.util.UUID;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    public static final int REQUEST_CAMERA = 1888;
    public static final int REQUEST_DETAILS = 1889;

    private ProgressWheel mPw;
    private Bitmap mPhoto;
    private Context mContext;
    private FaceRectangle mRect;
    private FloatingActionButton mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.log(TAG, "onCreate");
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Log in");
        mContext = getBaseContext();
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mPw = (ProgressWheel) findViewById(R.id.pw);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        });
    }

    private void cont() {
        Logger.log(TAG, "cont");
        mFab.setVisibility(View.GONE);
        mPw.setVisibility(View.VISIBLE);
        FaceModule.detect(mPhoto, new Next() {
            @Override
            public void next(Object obj) {
                Face [] faces = (Face []) obj;
                if (faces == null || faces.length == 0) {
                    Util.toast(mContext, "No faces found! Please retry.");
                    mFab.setVisibility(View.VISIBLE);
                    mPw.setVisibility(View.GONE);
                    return;
                }
                Face face = faces[0];
                UUID faceId = face.faceId;
                mRect = face.faceRectangle;
                FaceModule.findSimilar(mContext, faceId, new Next() {
                    @Override
                    public void next(Object obj) {
                        SimilarPersistedFace[] result = (SimilarPersistedFace[]) obj;
                        if (result != null && result.length > 0) {
                            final SimilarPersistedFace face = result[0];
                            FaceModule.getUser(mContext, face.persistedFaceId, new Next() {
                                @Override
                                public void next(Object obj) {
                                    JSONObject json = (JSONObject) obj;
                                    if (json == null) {
                                        mFab.setVisibility(View.VISIBLE);
                                        mPw.setVisibility(View.GONE);
                                        Util.toast(mContext, "Error! Please retry.");
                                        return;
                                    }
                                    User user = User.getInstance(mContext);
                                    user.setUserId(face.persistedFaceId.toString());
                                    try {
                                        user.setName(json.getString("name"));
                                        user.setAddress(json.getString("address"));
                                        user.setStatus(json.getString("status"));
                                        user.setPhone(json.getString("phone"));
                                        user.setEmail(json.getString("email"));
                                        Util.toast(mContext, "Welcome, " + user.getName() + "!");
                                    } catch (Exception exc) {
                                        exc.printStackTrace();
                                    }
                                    user.persist(mContext);
                                    mFab.setVisibility(View.VISIBLE);
                                    mPw.setVisibility(View.GONE);
                                    Intent i = new Intent(LoginActivity.this, OptionsActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(i);
                                }
                            });

                        } else {
                            mFab.setVisibility(View.VISIBLE);
                            mPw.setVisibility(View.GONE);
                            Intent i = new Intent(LoginActivity.this, DetailsActivity.class);
                            startActivityForResult(i, REQUEST_DETAILS);
                        }
                    }
                });
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.log(TAG, "onActivityResult");
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            mPhoto = photo;
            cont();
        } else if (requestCode == REQUEST_DETAILS && resultCode == Activity.RESULT_OK) {
            FaceModule.addFace(mContext, mPhoto, User.getInstance(mContext).getJson(), mRect, new Next() {
                @Override
                public void next(Object obj) {
                    User user = User.getInstance(mContext);
                    AddPersistedFaceResult res = (AddPersistedFaceResult) obj;
                    user.setUserId(res.persistedFaceId.toString());
                    user.persist(mContext);
                    Intent i = new Intent(LoginActivity.this, OptionsActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
            });
        }
    }

}
