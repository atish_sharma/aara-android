package com.aara.aara.controller.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.FaceModule;
import com.aara.aara.module.Logger;
import com.aara.aara.module.Next;
import com.aara.aara.module.Util;
import com.microsoft.projectoxford.face.contract.AddPersistedFaceResult;
import com.microsoft.projectoxford.face.contract.Face;
import com.microsoft.projectoxford.face.contract.FaceRectangle;
import com.microsoft.projectoxford.face.contract.SimilarPersistedFace;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONObject;

import java.util.UUID;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = "HomeActivity";

    public static final int REQUEST_CAMERA = 1888;

    private ProgressWheel mPw;
    private Bitmap mPhoto;
    private Context mContext;
    private FloatingActionButton mFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.log(TAG, "onCreate");
        setContentView(R.layout.activity_home);
        getSupportActionBar().setTitle("Who is this?");
        mContext = getBaseContext();
        mFab = (FloatingActionButton) findViewById(R.id.fab);
        mPw = (ProgressWheel) findViewById(R.id.pw);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
            }
        });
    }

    private void cont() {
        Logger.log(TAG, "cont");
        mFab.setVisibility(View.GONE);
        mPw.setVisibility(View.VISIBLE);
        FaceModule.detect(mPhoto, new Next() {
            @Override
            public void next(Object obj) {
                Face[] faces = (Face []) obj;
                if (faces == null || faces.length == 0) {
                    Util.toast(mContext, "No faces found! Please retry.");
                    mFab.setVisibility(View.VISIBLE);
                    mPw.setVisibility(View.GONE);
                    return;
                }
                Face face = faces[0];
                final UUID faceId = face.faceId;
                FaceModule.findSimilar(mContext, faceId, new Next() {
                    @Override
                    public void next(Object obj) {
                        SimilarPersistedFace[] result = (SimilarPersistedFace[]) obj;
                        if (result != null && result.length > 0) {
                            final SimilarPersistedFace face = result[0];
                            FaceModule.getUser(mContext, face.persistedFaceId, new Next() {
                                @Override
                                public void next(Object obj) {
                                    JSONObject json = (JSONObject) obj;
                                    if (json == null) {
                                        mFab.setVisibility(View.VISIBLE);
                                        mPw.setVisibility(View.GONE);
                                        Util.toast(mContext, "Error! Please retry.");
                                        return;
                                    }
                                    try {
                                        json.put("id", faceId.toString());
                                        String info = json.toString();
                                        Intent i = new Intent(HomeActivity.this, UserProfileActivity.class);
                                        i.putExtra("info", info);
                                        startActivity(i);
                                        /*
                                        String name = json.getString("name");
                                        String address = json.getString("address");
                                        String status = json.getString("status");
                                        String phone = json.getString("phone");
                                        String email = json.getString("email");
                                        String [] items = {
                                                "Phone   : " + phone,
                                                "Email   : " + email,
                                                "Address : " + address,
                                                "Status  : " + status
                                        };
                                        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                                        builder.setTitle(name).setItems(items, new DialogInterface.OnClickListener() {

                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                            }

                                        });
                                        builder.show();
                                        */
                                    } catch (Exception exc) {
                                        exc.printStackTrace();
                                    }
                                    mFab.setVisibility(View.VISIBLE);
                                    mPw.setVisibility(View.GONE);
                                }
                            });

                        } else {
                            mFab.setVisibility(View.VISIBLE);
                            mPw.setVisibility(View.GONE);
                            Util.toast(mContext, "This user does not exist in our database! Please try later or with another picture.");
                        }
                    }
                });
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.log(TAG, "onActivityResult");
        if (requestCode == REQUEST_CAMERA && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            mPhoto = photo;
            cont();
        }
    }

}

