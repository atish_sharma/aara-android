package com.aara.aara.controller.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.Logger;

public class DetailsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setTitle("Sign up");
        findViewById(R.id.button_su).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.log("DetailsActivity", "onSignUp");
                User newUser = User.getInstance(getApplicationContext());
                EditText ed1 = (EditText) findViewById(R.id.editText);
                newUser.setName(ed1.getText().toString());
                EditText ed2 = (EditText) findViewById(R.id.editText2);
                newUser.setPhone(ed2.getText().toString());
                EditText ed3 = (EditText) findViewById(R.id.editText3);
                newUser.setEmail(ed3.getText().toString());
                EditText ed4 = (EditText) findViewById(R.id.editText4);
                newUser.setStatus(ed4.getText().toString());
                EditText ed5 = (EditText) findViewById(R.id.editText5);
                newUser.setAddress(ed5.getText().toString());
                newUser.persist(getBaseContext());
                Intent resultIntent = new Intent();
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
    }
}
