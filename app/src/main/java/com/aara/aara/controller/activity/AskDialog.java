package com.aara.aara.controller.activity;

        import android.app.Dialog;
        import android.os.Bundle;
        import android.support.v7.app.AppCompatActivity;
        import android.view.View;
        import android.view.Window;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.TextView;

        import com.aara.aara.R;
        import com.aara.aara.module.Next;


public class AskDialog extends Dialog implements android.view.View.OnClickListener {

    private static final String TAG = "RedeemCouponDialog";

    private EditText txt;
    private Next n;

    public AskDialog(AppCompatActivity activity, Next nxt) {
        super(activity);
        n = nxt;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.ask_dialog);
        txt = (EditText) findViewById(R.id.input);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_pos:
                String t = txt.getText().toString();
                n.next(t);
                break;
            case R.id.btn_off:
            default:
                break;
        }
        dismiss();
    }

}
