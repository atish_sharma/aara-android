package com.aara.aara.controller.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.FaceModule;
import com.aara.aara.module.Logger;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivity";

    private AppCompatActivity mActivity;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.log(TAG, "onCreate");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        mActivity = this;
        mContext = getBaseContext();
        FaceModule.init(mContext);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                User user = User.getInstance(mContext);
                Intent i = null;
                if (user.loggedIn()) {
                    i = new Intent(SplashActivity.this, OptionsActivity.class);
                } else {
                    i = new Intent(SplashActivity.this, LoginActivity.class);
                }
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        }, getResources().getInteger(R.integer.splash_screen_time));
    }
}
