package com.aara.aara.controller.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;

import com.aara.aara.R;
import com.aara.aara.model.User;
import com.aara.aara.module.Logger;

public class OptionsActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    public static final int REQUEST_CAMERA = 1888;
    /*
    private Button mUploadPhoto;
    private Button mUseAppliance;
    private Button mTakeMeHome;
    private Context mContext;
    */
    private LinearLayout mL1, mL2, mL3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.log(TAG, "onCreate");
        setContentView(R.layout.activity_options);
        getSupportActionBar().setTitle("Options");
        Intent intent = getIntent();

        if(intent.getStringArrayExtra("productList") != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(OptionsActivity.this);
            Logger.log(TAG, "Mai yaha");
            builder.setTitle("Names Of Products").setItems(intent.getStringArrayExtra("productList"), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }

            });
            builder.show();
        }
        /*
        mContext = getBaseContext();
        mUploadPhoto = (Button) findViewById(R.id.btn_upload_photo);
        mUseAppliance = (Button) findViewById(R.id.btn_use_appliance);
        mTakeMeHome = (Button) findViewById(R.id.btn_take_me_home);
        */
        mL1 = (LinearLayout) findViewById(R.id.l1);
        mL2 = (LinearLayout) findViewById(R.id.l2);
        mL3 = (LinearLayout) findViewById(R.id.l3);

        //what is this?
        mL1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(OptionsActivity.this, ObjectDetectionActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

        //who is this?
        mL2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OptionsActivity.this, HomeActivity.class));
            }
        });

        //guide me home
        mL3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User currentUser = User.getInstance(getApplicationContext());
                String address = currentUser.getAddress();
                address = address.replaceAll(" ","+");
                address = address.replaceAll(",","+");
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+address);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        /*
        mUploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mUseAppliance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mTakeMeHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User currentUser = User.getInstance(getApplicationContext());
                String address = currentUser.getAddress();
                address = address.replaceAll(" ","+");
                address = address.replaceAll(",","+");
                Uri gmmIntentUri = Uri.parse("google.navigation:q="+address);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                startActivity(mapIntent);
            }
        });
        */
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Logger.log(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Logger.log(TAG, "onOptionsItemSelected");
        int id = item.getItemId();
        if (id == R.id.logout) {
            User user = User.getInstance(getBaseContext());
            user.logout(getBaseContext());
            Intent i = new Intent(OptionsActivity.this, LoginActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

}
